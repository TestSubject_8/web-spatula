#	IMPORTS
from Tkinter import *
import webbrowser

import scrape

#	FUNCTIONS
def butcont():
	if button["text"]=="Waht":
		button['text']="Now what, eh?"
	else:
		button["text"]="Waht"
def cfigDisplay():
	f.bg="black"
	f.fg="white"
	button["fg"]="white"
	button["bg"]="black"
	if(scrape.new==True):
		read["bg"]='red'
		read['fg']="black"
def readArticle():
	webbrowser.open(scrape.url)
def quit(arg):
	exit()

#	INIT
f = Tk()
info = Label(f)
button = Button(text="Click me!",command=butcont)
read = Button(text="Read article",command=readArticle)
cfigDisplay()

#	EXECUTION
info['text'] = scrape.title
f.grid()
button.grid(row=2,column=3)
read.grid(row=2,column=1)
info.grid(row=1,column=1)

'''for i in soup.find('title'):
	print i.string
info['text'] = i.string
info.grid(row=2,column=1)'''

f.bind('<Escape>',quit)

f.mainloop()
